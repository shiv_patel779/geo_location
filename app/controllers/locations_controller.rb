class LocationsController < ApplicationController

  def index
    @locations = Location.all
  end

  def new
  end

  def create
    coordinates= get_cordinates(params[:address])
    suggested_bounds =Geokit::Geocoders::GoogleGeocoder.geocode('140 Market St, San Francisco, CA').suggested_bounds    

    @location = Location.create( 
      name: params[:address], 
      area: "POLYGON((0 0,
        #{suggested_bounds.sw.lat} #{suggested_bounds.sw.lng},
      #{suggested_bounds.ne.lat} #{suggested_bounds.ne.lng},
      #{coordinates["lat"]} #{coordinates["lng"]},
      0 0))"
    )
    country_name= @location.name.split(",").pop().strip    
    detail = ISO3166::Country.find_country_by_name(country_name)
    @country = detail.data["alpha3"]
  end

  def show
    @location = Location.find_by( id: params[:id] )
  end

  private

    def get_cordinates address      
      encoded_url = URI.encode("https://maps.googleapis.com/maps/api/geocode/json?address=#{address}&key=AIzaSyCRGIfFKIAdU_egYO3H9rD13DVSOB9sx0I")
      uri = URI.parse( encoded_url )
      response = Net::HTTP.get(uri)

      return JSON.parse(response)["results"][0]["geometry"]["location"]
    end

end
